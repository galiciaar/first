#ifndef FIGURAS_H
#define FIGURAS_H


class Figuras
{
    public:
        Figuras(int lados, int caras, int aristas, int vertices, int angulo, int volumen, int area, string simetrico, string asimetrico, string formula);
        virtual ~Figuras();

        int Get_lados() { return _lados; }
        void Set_lados(int val) { _lados = val; }
        int Get_caras() { return _caras; }
        void Set_caras(int val) { _caras = val; }
        int Get_aristas() { return _aristas; }
        void Set_aristas(int val) { _aristas = val; }
        int Get_vertices() { return _vertices; }
        void Set_vertices(int val) { _vertices = val; }
        int Get_angulo() { return _angulo; }
        void Set_angulo(int val) { _angulo = val; }
        int Get_volumen() { return _volumen; }
        void Set_volumen(int val) { _volumen = val; }
        int Get_area() { return _area; }
        void Set_area(int val) { _area = val; }
        string Get_simetrico() { return _simetrico; }
        void Set_simetrico(string val) { _simetrico = val; }
        string Get_asimetrico() { return _asimetrico; }
        void Set_asimetrico(string val) { _asimetrico = val; }
        string Get_formula() { return _formula; }
        void Set_formula(string val) { _formula = val; }

    protected:

    private:
        int _lados;
        int _caras;
        int _aristas;
        int _vertices;
        int _angulo;
        int _volumen;
        int _area;
        string _simetrico;
        string _asimetrico;
        string _formula;
};

#endif // FIGURAS_H
