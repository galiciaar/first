#ifndef GUITARRAS_H
#define GUITARRAS_H


class Guitarras
{
    public:
        Guitarras(int cuerdas, int trastes, string acustico, string electrico, string cuerpo, string diapason, string pastillas, string puente, string peso, string madera);
        virtual ~Guitarras();

        int Get_cuerdas() { return _cuerdas; }
        void Set_cuerdas(int val) { _cuerdas = val; }
        int Get_trastes() { return _trastes; }
        void Set_trastes(int val) { _trastes = val; }
        string Get_acustico() { return _acustico; }
        void Set_acustico(string val) { _acustico = val; }
        string Get_electrico() { return _electrico; }
        void Set_electrico(string val) { _electrico = val; }
        string Get_cuerpo() { return _cuerpo; }
        void Set_cuerpo(string val) { _cuerpo = val; }
        string Get_diapason() { return _diapason; }
        void Set_diapason(string val) { _diapason = val; }
        string Get_pastillas() { return _pastillas; }
        void Set_pastillas(string val) { _pastillas = val; }
        string Get_puente() { return _puente; }
        void Set_puente(string val) { _puente = val; }
        string Get_peso() { return _peso; }
        void Set_peso(string val) { _peso = val; }
        string Get_madera() { return _madera; }
        void Set_madera(string val) { _madera = val; }

    protected:

    private:
        int _cuerdas;
        int _trastes;
        string _acustico;
        string _electrico;
        string _cuerpo;
        string _diapason;
        string _pastillas;
        string _puente;
        string _peso;
        string _madera;
};

#endif // GUITARRAS_H
