#ifndef MOTORES_H
#define MOTORES_H


class Motores
{
    public:
        Motores(int rpm, int, fuerza, int velocidad, int control, int temperatura, int consumo, int costo, string daņoambiente, string durabilidad, string servicio );
        virtual ~Motores();

        int_ Getrpm() { return rpm; }
        void Setrpm(int_ val) { rpm = val; }
        int_ Getfuerza() { return fuerza; }
        void Setfuerza(int_ val) { fuerza = val; }
        int Get_velocidad() { return _velocidad; }
        void Set_velocidad(int val) { _velocidad = val; }
        int Get_control() { return _control; }
        void Set_control(int val) { _control = val; }
        int Get_temperatura() { return _temperatura; }
        void Set_temperatura(int val) { _temperatura = val; }
        int Get_consumo() { return _consumo; }
        void Set_consumo(int val) { _consumo = val; }
        int Get_costo() { return _costo; }
        void Set_costo(int val) { _costo = val; }
        string Get_daņoambiente() { return _daņoambiente; }
        void Set_daņoambiente(string val) { _daņoambiente = val; }

    protected:

    private:
        int_ rpm;
        int_ fuerza;
        int _velocidad;
        int _control;
        int _temperatura;
        int _consumo;
        int _costo;
        string _daņoambiente;
};

#endif // MOTORES_H
