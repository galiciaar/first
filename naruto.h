#ifndef NARUTO_H
#define NARUTO_H


class Naruto
{
    public:
        Naruto(int tecnicas, int fuerza, int velocidad, int control, int inteligencia, int trabajoequipo, string clan, string armas, string naturaleza, string aptitudes);
        virtual ~Naruto();

        int Get_tecnicas() { return _tecnicas; }
        void Set_tecnicas(int val) { _tecnicas = val; }
        int Get_fuerza() { return _fuerza; }
        void Set_fuerza(int val) { _fuerza = val; }
        int Get_velocidad() { return _velocidad; }
        void Set_velocidad(int val) { _velocidad = val; }
        int Get_control() { return _control; }
        void Set_control(int val) { _control = val; }
        int Get_inteligencia() { return _inteligencia; }
        void Set_inteligencia(int val) { _inteligencia = val; }
        int Get_trabajoequipo() { return _trabajoequipo; }
        void Set_trabajoequipo(int val) { _trabajoequipo = val; }
        string Get_clan() { return _clan; }
        void Set_clan(string val) { _clan = val; }
        string Get_armas() { return _armas; }
        void Set_armas(string val) { _armas = val; }
        string Get_naturaleza() { return _naturaleza; }
        void Set_naturaleza(string val) { _naturaleza = val; }
        string Get_aptitudes() { return _aptitudes; }
        void Set_aptitudes(string val) { _aptitudes = val; }

    protected:

    private:
        int _tecnicas;
        int _fuerza;
        int _velocidad;
        int _control;
        int _inteligencia;
        int _trabajoequipo;
        string _clan;
        string _armas;
        string _naturaleza;
        string _aptitudes;
};

#endif // NARUTO_H
