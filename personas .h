#ifndef PERSONAS _H
#define PERSONAS _H


class Personas
{
    public:
        Personas (int edad, int altura, int talla, int peso, string nombre, string piel, string ojos, string pelo, string nariz, string sobrenombre);
        virtual ~Personas ();

        int Get_altura() { return _altura; }
        void Set_altura(int val) { _altura = val; }
        int Get_edad() { return _edad; }
        void Set_edad(int val) { _edad = val; }
        int Get_talla() { return _talla; }
        void Set_talla(int val) { _talla = val; }
        int Get_peso() { return _peso; }
        void Set_peso(int val) { _peso = val; }
        string Get_nombre() { return _nombre; }
        void Set_nombre(string val) { _nombre = val; }
        string Get_piel() { return _piel; }
        void Set_piel(string val) { _piel = val; }
        string Get_ojos() { return _ojos; }
        void Set_ojos(string val) { _ojos = val; }
        string Get_pelo() { return _pelo; }
        void Set_pelo(string val) { _pelo = val; }
        string Get_nariz() { return _nariz; }
        void Set_nariz(string val) { _nariz = val; }
        string Get_sobrenombre() { return _sobrenombre; }
        void Set_sobrenombre(string val) { _sobrenombre = val; }

    protected:

    private:
        int _altura;
        int _edad;
        int _talla;
        int _peso;
        string _nombre;
        string _piel;
        string _ojos;
        string _pelo;
        string _nariz;
        string _sobrenombre;
};

#endif // PERSONAS _H
